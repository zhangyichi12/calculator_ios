//
//  ViewController.swift
//  MyCalculator
//
//  Created by Yichi Zhang on 10/25/15.
//  Copyright © 2015 Golden. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var display: UILabel!
    
    var userInTheMiddleOfTyping = false
    @IBAction func appendDigit(sender: UIButton) {
        sender.backgroundColor = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1)
        let digit = sender.currentTitle!
        if(userInTheMiddleOfTyping && display.text != "0") {
            display.text = display.text! + digit
        }
        else {
            display.text = digit
            userInTheMiddleOfTyping = true
            
        }
    }
    @IBAction func appendDigitFinished(sender: UIButton) {
        sender.backgroundColor = UIColor(red: 74/255, green: 157/255, blue: 251/255, alpha: 1)
    }
    
    var operand = [Double]()
    
    var displayValue: Double {
        get {
            return NSNumberFormatter().numberFromString(display.text!)!.doubleValue
        }
        set {
            display.text = "\(newValue)"
        }
    }
    
    
    @IBAction func enter(sender: UIButton) {
        sender.backgroundColor = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1)
        display.textColor = UIColor.grayColor()
        
        operand.append(displayValue)
        
        userInTheMiddleOfTyping = false
        
        print(operand)
    }
    
    @IBAction func enterFinished(sender: UIButton) {
        sender.backgroundColor = UIColor.whiteColor()
        display.textColor = UIColor.blackColor()
    }

}

